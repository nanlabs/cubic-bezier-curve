# Cubic Bezier Curve exercise

[LIVE VERSION](https://cubic-bezier-curve-skuibhibal.now.sh)

## Tasks

* [DONE] - Init d3 and draw basic path element
* [DONE] - Draw a cubic bezier curve with endpoints in diagonal corners and with arbitrary control points.
* [DONE] - Extend curve to fit to a browser viewport 
* [DONE] - Place N elements along the bezier curve. 
* [DONE] - The elements should be equally spaced. 
* [DONE] -Use a slider to set N from 1 to 100.
* [DONE] -Use a second slider to draw multiple similar curves from 1 to 100. 
* [DONE] - The curves should redraw when the browser is resized and when the sliders are moved.
* [DONE] - Performance improvements (use canvas instead of d3 & double check if debounce is needed)

## Local setup

You need to have node and npm installed locally. 

```
cd cubic-bezier-curve
npm i
npm start
```

## Serverless Deployment

Install [now](https://zeit.co/download) and deploy with one command.

```
cd cubic-bezier-curve
now
```
