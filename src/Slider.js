import React, { Component } from 'react';

import './Slider.css';

class Slider extends Component {

  render() {

    return (
      <div className='slider-container'>
        <span className='legend'><b>{this.props.title}</b>: {this.props.value}</span>
        <input className='slider' type='range' min='1' max='100' value={this.props.value} onChange={this.props.onchange} />
      </div>
    );
  }
  
}

export default Slider;