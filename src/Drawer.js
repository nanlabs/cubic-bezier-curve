const TWO_PI = 2 * Math.PI; 

class Drawer {
  
  constructor(context, width, height) {
    this.width = width;
    this.height = height;
    this.ctx = context;
  }

  clear() {
    this.ctx.clearRect(0, 0, this.width, this.height);
  }

  bezierCurve(controlPoints, styles) {
    this.ctx.beginPath();
    this.ctx.moveTo(controlPoints[0].x, controlPoints[0].y);
    this.ctx.bezierCurveTo(controlPoints[1].x, controlPoints[1].y, controlPoints[2].x, controlPoints[2].y, controlPoints[3].x, controlPoints[3].y);
    this.ctx.strokeStyle = styles.strokeStyle;
    this.ctx.stroke();
  }

  circle(center, radius, styles){
    this.ctx.beginPath();
    this.ctx.arc(center.x, center.y, radius, 0, TWO_PI, false);
    this.ctx.fillStyle = styles.fillStyle;
    this.ctx.strokeStyle = styles.strokeStyle;
    this.ctx.fill();
    this.ctx.stroke();
  }

}

export default Drawer;