import  React, { Component } from 'react';
import * as d3 from 'd3';
import { elements } from './Utils.js';
import Drawer from './Drawer';

import './GraphContainer.css';

const CURVE_COLOR = 'gray';
const ENDPOINT_COLOR = '#474747';
const CIRCLE_COLOR = '#5d73f5';

class GraphContainer extends Component {

  render() {

    return (
      <div id='canvasContainer' className='canvas-container'></div>
    );
  }

  componentDidMount() {

    this.canvas = d3.select('#canvasContainer').append('canvas');
    this.ctx = this.canvas.node().getContext('2d');
    this.drawer = new Drawer(this.ctx);
    this.resizeCanvas();
    this.draw();
  }

  componentDidUpdate() {

    this.resizeCanvas();
    this.draw();
  }

  draw() {

    const {curves, elements} = this.props;
    this.drawer.clear();
    d3.range(curves)
      .forEach( d => {
        const cp = this.buildControlPoints(d, curves);
        this.drawCurve(cp);
        this.drawElements(elements, cp);
      });
    
  }

  drawCurve(cp) {

    this.drawer.bezierCurve(cp, {strokeStyle: CURVE_COLOR});
    this.drawer.circle(cp[0], 3, {fillStyle: ENDPOINT_COLOR, strokeStyle: ENDPOINT_COLOR});
    this.drawer.circle(cp[3], 3, {fillStyle: ENDPOINT_COLOR, strokeStyle: ENDPOINT_COLOR});
  }

  drawElements(size, cp) {
    elements(cp, size).forEach( p => {
      this.drawer.circle(p, 4, {fillStyle: CIRCLE_COLOR, strokeStyle: CIRCLE_COLOR})
    });
  }

  resizeCanvas() {

    const {width, height} = this.props;
    this.canvas
      .attr('width', width)
      .attr('height', height);
    this.drawer.width = width;
    this.drawer.height = height;
  }


  buildControlPoints(idx, size) {

    const margin = 20;
    const w = this.props.width - margin;
    const h = this.props.height - margin;
    const offset =(idx * (h / size));
    return [
      { x: margin, y: margin + offset },
      { x: w / 8, y: h / 5 },
      { x: w - w / 2, y: h - h / 5 },
      { x: w, y: h - offset }
    ]
  }

}
export default GraphContainer;