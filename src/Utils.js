export function getDimensions() {

  const html = document.documentElement;
  return { height: html.clientHeight, width: html.clientWidth };
}

export function Bezier(cp0, cp1, cp2, cp3) {
  
  const cx = 3.0 * (cp1.x - cp0.x);
  const bx = 3.0 * (cp2.x - cp1.x) - cx;
  const ax = cp3.x - cp0.x - cx - bx;
  
  const cy = 3.0 * (cp1.y - cp0.y);
  const by = 3.0 * (cp2.y - cp1.y) - cy;
  const ay = cp3.y - cp0.y - cy - by;
  
  return function (t) {
    const tSquared = t * t;
    const tCubed = tSquared * t;
    return {
      x: (ax * tCubed) + (bx * tSquared) + (cx * t) + cp0.x,
      y: (ay * tCubed) + (by * tSquared) + (cy * t) + cp0.y
    };
  } 
}

export function elements(controlPoints, size) {
  
  const bezier = Bezier(...controlPoints);
  const n = Math.round(distanceOf(controlPoints[0], controlPoints[3]));
  const s = 1 / (n - 1);
  const elements = new Array(n);
  let curveLength = 0;
  
  for(let i = 0; i < n; i++) {
    const t = i * s;
    const point = bezier(t);
    const distance = i > 0 ? distanceOf( point, elements[i - 1].point) : 0;
    curveLength += distance;
    elements[i] = {t, point, distance, curveLength};
  }

  for(let j = 0; j < n; j++) {
    elements[j].r = elements[j].curveLength / curveLength;
  }

  const step = 1 / (size + 1);
  const result = new Array(size);
  
  for(let k = 0; k < size; k++) {
    result[k] = findClosestPoint(elements, (k + 1) * step);
  }

  return result;
}

function distanceOf(p1, p2) {
  return Math.sqrt((Math.pow(p2.x - p1.x, 2) + Math.pow(p2.y - p1.y, 2)));
}

function findClosestPoint(elements, r) {
  for(let i = 0; i < elements.length; i++) {
    if( elements[i].r > r) {
      const diff = elements[i].r - r;
      const diffPrevious = r - elements[i - 1].r;
      const closest = i > 0 && diffPrevious < diff ? elements[i - 1]: elements[i]; 
      return closest.point;
    }
  }
}