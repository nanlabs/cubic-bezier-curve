import React, { Component } from 'react';
import { debounce } from 'debounce';
import GraphContainer from './GraphContainer';
import Slider from './Slider';
import { getDimensions } from './Utils.js';

import './App.css';

class App extends Component {

  constructor(props) {

    super(props);
    const dimensions = getDimensions();

    this.state = {
      height: dimensions.height,
      width: dimensions.width,
      curves: 1,
      elements: 2
    };

    window.addEventListener('resize', this.updateDimensions);
  }

  updateDimensions = () => {
    debounce(this.setState(getDimensions()), 100);
  }

  setCurves = (e) => {

    this.setState({
      curves: parseInt(e.target.value, 10)
    });
  }

  setElements = (e) => {

    this.setState({
      elements: parseInt(e.target.value, 10)
    });
  }

  render() {

    return (
      <div className='App'>
        <div className='App-intro'>
          <GraphContainer height={this.state.height} width={this.state.width} curves={this.state.curves} elements={this.state.elements} />
        </div>
        <div className='controls-container'>
          <Slider title='Curves' value={this.state.curves} onchange={this.setCurves} />
          <Slider title='Elements' value={this.state.elements} onchange={this.setElements} />
        </div>
      </div>
    );
  }

}

export default App;
